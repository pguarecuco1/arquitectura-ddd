﻿using GloiathNationalBank.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace GloiathNationalBank.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Transactions> Transactions { get; set; }

        public DbSet<Rates> Rates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          
        }
    }
}