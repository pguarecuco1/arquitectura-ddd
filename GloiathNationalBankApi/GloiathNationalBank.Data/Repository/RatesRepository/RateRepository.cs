﻿using GloiathNationalBank.Application.Contracts.IRepository;
using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Infrastructure.Repository.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Infrastructure.Repository.RatesRepository
{
    public class RateRepository: IRateRepository
    {
        private readonly IRateProviderRepository _rateExternalProvider;
        private readonly ApplicationDbContext _context;
        private readonly ILogger<RateRepository> _logger;

        public RateRepository(IRateProviderRepository rateExternalProvider,
            ApplicationDbContext context,
            ILogger<RateRepository> logger)
        {
            _rateExternalProvider = rateExternalProvider;
            _context = context;
            _logger = logger;
        }

        public async Task<IEnumerable<Rates>> GetAll()
        {
            IEnumerable<Rates> rates = new List<Rates>();
            try
            {
                rates = await _rateExternalProvider.GetAllRatesProvider();
                await RefreshAlternativeStorage(rates);
                return rates;
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed retrieve Rates from WebService ", ex);

            }

            _logger.LogInformation("Retrieving rates from alternative storage");
            rates = await GetAllRatesFromAlternativeStorage();
            return rates;
        }

        private async Task<IEnumerable<Rates>> GetAllRatesFromAlternativeStorage()
        {
            return (IEnumerable<Rates>)await _context.Rates.ToListAsync();
        }

        private async Task RefreshAlternativeStorage(IEnumerable<Rates> ratesFromWebService)
        {
            await DeleteAll();
            await Insert(ratesFromWebService);
        }

        private async Task DeleteAll()
        {
            var allRates = await GetAllRatesFromAlternativeStorage();
            _context.Rates.RemoveRange(allRates);
            await _context.SaveChangesAsync();
        }

        private async Task Insert(IEnumerable<Rates> ratesFromWebService)
        {
            await _context.Rates.AddRangeAsync(ratesFromWebService);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Domain.Entities.Rates>> GetAllDb()
        { 
            return await _context.Rates.ToListAsync();
        }
    }
}
