﻿using GloiathNationalBank.Application.Contracts.IRepository;
using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Infrastructure.Repository.Providers;
using GloiathNationalBank.Infrastructure.Repository.RatesRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Infrastructure.Repository.Transactions
{
    public class TransactionRepository : ITransactionRepository
    {
        private readonly ITransactionProviderRepository _transactionExternalProvider;
        private readonly ILogger<RateRepository> _logger;
        private readonly ApplicationDbContext _context;

        public TransactionRepository(ITransactionProviderRepository transactionExternalProvider,
            ApplicationDbContext context,
            ILogger<RateRepository> logger)
        {
            _context = context;
            _transactionExternalProvider = transactionExternalProvider;
            _logger = logger;
        }

        public async Task<IEnumerable<Domain.Entities.Transactions>> GetAll()
        {
            IEnumerable<Domain.Entities.Transactions> transactions = new List<Domain.Entities.Transactions>();
            try
            {
                transactions = await _transactionExternalProvider.GetAllTransactions();
                await RefreshAlternativeStorage(transactions);
                return transactions;
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed retrieve Transactions from WebService ", ex);
            }

            _logger.LogInformation("Retrieving transactions from alternative storage");
            transactions = await GetAllTransactionsFromAlternativeStorage();
            return transactions;
        }

        public async Task<IEnumerable<Domain.Entities.Transactions>> GetId(string sku)
        {
            return await _context.Transactions.Where(x => x.Sku == sku).ToListAsync();
        }
        private async Task<IEnumerable<Domain.Entities.Transactions>> GetAllTransactionsFromAlternativeStorage()
        {
            return await _context.Transactions.ToListAsync();
        }


        private async Task RefreshAlternativeStorage(IEnumerable<Domain.Entities.Transactions> ratesFromWebService)
        {
            await DeleteAll();
            await Insert(ratesFromWebService);
        }

        private async Task DeleteAll()
        {
            var allTransactions = await GetAllTransactionsFromAlternativeStorage();
            _context.Transactions.RemoveRange(allTransactions);
            await _context.SaveChangesAsync();
        }

        private async Task Insert(IEnumerable<Domain.Entities.Transactions> transactionsFromWebService)
        {
            await _context.Transactions.AddRangeAsync(transactionsFromWebService);
            await _context.SaveChangesAsync();
        }       
     
    }
}
