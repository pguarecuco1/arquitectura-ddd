﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Infrastructure.Repository.Providers
{
    public interface ITransactionProviderRepository
    {
        Task<IEnumerable<Domain.Entities.Transactions>> GetAllTransactions();
    }
}
