﻿using GloiathNationalBank.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Infrastructure.Repository.Providers
{
    public class TransactionProviderRepository: ITransactionProviderRepository
    {
        private readonly HttpClient _httpClient;

        public TransactionProviderRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Domain.Entities.Transactions>> GetAllTransactions()
        {
            using (var response = await _httpClient.GetAsync("transactions.json"))
            {
                response.EnsureSuccessStatusCode();
                var responseAsString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IList<Domain.Entities.Transactions>>(responseAsString);
            }
        }
    }
}
