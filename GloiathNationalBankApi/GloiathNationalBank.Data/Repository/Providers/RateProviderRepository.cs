﻿using GloiathNationalBank.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Infrastructure.Repository.Providers
{
    public class RateProviderRepository: IRateProviderRepository
    {
        private readonly HttpClient _httpClient;

        public RateProviderRepository(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<Rates>> GetAllRatesProvider()
        {
            using (var response = await _httpClient.GetAsync("rates.json"))
            {
                response.EnsureSuccessStatusCode();
                var responseAsString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IList<Rates>>(responseAsString);
            }
        }
    }
}
