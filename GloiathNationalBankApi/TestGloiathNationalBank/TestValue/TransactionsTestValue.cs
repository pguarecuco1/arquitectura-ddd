﻿using GloiathNationalBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGloiathNationalBank.TestValue
{
    public class TransactionsTestValue
    {
        public static IEnumerable<TransactionModel> GetTestTransactions()
        {
            var transactions = new List<TransactionModel>()
            {
                new TransactionModel("T2006", 10.00M, "USD"),
                 new TransactionModel("M2007", 34.57M, "CAD"),
                 new TransactionModel("R2008", 17.95M, "USD"),
                 new TransactionModel("T2006", 7.63M, "EUR"),
                 new TransactionModel("B2009", 21.23M, "USD")
            };

            return transactions;
        }

    }
}
