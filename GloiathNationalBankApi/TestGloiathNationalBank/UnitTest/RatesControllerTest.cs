﻿using AutoMapper;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Controllers;
using GloiathNationalBank.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestGloiathNationalBank.TestValue;

namespace TestGloiathNationalBank.UnitTest
{
    [TestClass]
    public class RatesControllerTest
    {
        private readonly ILogger<InternationalBusinessController> _logger;
        private readonly IRateService _rateService;
        private readonly ITransactionService _transactionService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        private readonly RatesTestValue _testRates;

        public RatesControllerTest(RatesTestValue testRates, ILogger<InternationalBusinessController> logger,
            IRateService rateService, ITransactionService transactionService, IProductService productService)
        {
            _logger = logger;
            _rateService = rateService;
            _transactionService = transactionService;
            _productService = productService;
            _testRates = testRates;
        }
        [TestMethod]
        public async Task RatesControllerGetAllOk()
            {
                //Arrange
             
            InternationalBusinessController controller = new InternationalBusinessController(_rateService, _transactionService, _productService, _mapper, _logger);

                //Act
                IActionResult result = await controller.GetRatesAll();
                ObjectResult? dtoAsObjectResult = result as ObjectResult;
                var dtoResult = dtoAsObjectResult.Value as IEnumerable<RateModel>;

                // Assert
                Assert.IsNotNull(dtoAsObjectResult);
                Assert.AreEqual(StatusCodes.Status200OK, dtoAsObjectResult.StatusCode);
                Assert.AreEqual(5, dtoResult.Count());
            }

            //[TestMethod]
            //public async Task RatesController_GetAll_Error()
            //{
            //    //Arrange
            //    _mockRateService.Setup(m => m.GetAll())
            //                                 .Throws(new Exception());

            //    RatesController controller = new RatesController(_mockRateService.Object, _mockMapper, _mockLogger.Object);

            //    //Act
            //    IActionResult result = await controller.GetAll();
            //    var internalServerErrorCode = result as StatusCodeResult;

            //    // Assert
            //    Assert.IsNotNull(result);
            //    Assert.AreEqual(result.GetType(), typeof(StatusCodeResult));
            //    Assert.AreEqual(StatusCodes.Status500InternalServerError, internalServerErrorCode.StatusCode);
            //}



        }
    }
