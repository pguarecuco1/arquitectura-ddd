﻿using GloiathNationalBank.Application.Models;
using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBankXUnitTest.MockData
{
    public class ProductTestValue
    {
        public static Product GetProductTransactionsDto(string productId)
        {
            var model = new Product();
            model.transaction = GetTransactionsByProduct(productId)
                                 .Select(t => new Transactions()
                                 {
                                     Amount = t.Amount,
                                     Currency = t.Currency,
                                     Sku = t.Sku
                                 })
                                 .ToList();

            model.total = model.transaction.Sum(x => x.Amount);
            return model;
        }
        internal static IEnumerable<Transactions> GetTransactionsByProduct(string productId)
        {
            return TransactionTestValue.GetAllTransactions().Where(x => x.Sku == productId);
        }
    }
}
