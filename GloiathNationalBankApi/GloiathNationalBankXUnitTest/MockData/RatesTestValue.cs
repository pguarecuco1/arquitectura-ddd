﻿using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBankXUnitTest.MockData
{
    public class RatesTestValue
    {
        internal static IEnumerable<Rates> GetAllRates()
        {
            var rates = new List<Rates>()
            {
                new Rates("EUR","USD", 1.359m),
                new Rates("CAD","EUR", 0.732m),
                new Rates("USD","EUR", 0.736m),
                new Rates("EUR","CAD", 0.732m),
                new Rates("CAD","EUR", 1.366m)
            };

            return rates;
        }
    }
}
