﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBankXUnitTest.MockData
{
    public class TransactionTestValue
    {
        public static IEnumerable<Transactions> GetAllTransactions()
        {
            var transactions = new List<Transactions>()
            {
                new Transactions("T2006", 10.00M, "USD"),
                 new Transactions("M2007", 34.57M, "CAD"),
                 new Transactions("R2008", 17.95M, "USD"),
                 new Transactions("T2006", 7.63M, "EUR"),
                 new Transactions("B2009", 21.23M, "USD")
            };

            return transactions;
        }
    }
}
