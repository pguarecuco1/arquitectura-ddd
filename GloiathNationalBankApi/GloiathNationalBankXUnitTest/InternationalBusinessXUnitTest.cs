using AutoMapper;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Application.Models;
using GloiathNationalBank.Controllers;
using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Mapper;
using GloiathNationalBank.Models;
using GloiathNationalBankXUnitTest.MockData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;

namespace GloiathNationalBankXUnitTest
{
    public class InternationalBusinessXUnitTest
    {
        private readonly Mock<ILogger<InternationalBusinessController>> _mockLogger;
        private readonly Mock<IRateService> _mockRateService;
        private readonly Mock<ITransactionService> _mockTransactionService;
        private readonly Mock<IProductService> _mockProductTransactionService;
        private readonly IMapper _mockMapper;

        public InternationalBusinessXUnitTest()
        {
            _mockLogger = new Mock<ILogger<InternationalBusinessController>>();
            _mockRateService = new Mock<IRateService>();
            _mockTransactionService = new Mock<ITransactionService>();
            _mockProductTransactionService = new Mock<IProductService>();

            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DtoMapper());
            });
            _mockMapper = mockMapper.CreateMapper();
        }      

        [Fact]
        public async Task GetRatesAsync_ReturnIActionResult()
        {
            //Arrange
            _mockRateService.Setup(m => m.GetAll())
                                         .Returns(Task.FromResult(RatesTestValue.GetAllRates()));
            _mockTransactionService.Setup(m => m.GetAll())
                                        .Returns(Task.FromResult(TransactionTestValue.GetAllTransactions()));
            _mockProductTransactionService.Setup(m => m.ConvertTransaction(It.IsAny<string>()))
                                       .Throws(new Exception());

            InternationalBusinessController controller = new InternationalBusinessController(_mockRateService.Object, _mockTransactionService.Object, _mockProductTransactionService.Object, _mockMapper, _mockLogger.Object);

            //Act
            var result = await controller.GetRatesAll();
            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task GetTransactionsAsync_ReturnIActionResult()
        {
            //Arrange
            _mockRateService.Setup(m => m.GetAll())
                                         .Returns(Task.FromResult(RatesTestValue.GetAllRates()));
            _mockTransactionService.Setup(m => m.GetAll())
                                        .Returns(Task.FromResult(TransactionTestValue.GetAllTransactions()));
            _mockProductTransactionService.Setup(m => m.ConvertTransaction(It.IsAny<string>()))
                                       .Throws(new Exception());

            InternationalBusinessController controller = new InternationalBusinessController(_mockRateService.Object, _mockTransactionService.Object, _mockProductTransactionService.Object, _mockMapper, _mockLogger.Object);

            //Act
            var result = await controller.GetTransactionsAll();
            //Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task ProductsController_GetTransactionsCollectionByProduct_Ok()
        {
            //Arrange
            _mockRateService.Setup(m => m.GetAll())
                                             .Returns(Task.FromResult(RatesTestValue.GetAllRates()));
            _mockTransactionService.Setup(m => m.GetAll())
                                        .Returns(Task.FromResult(TransactionTestValue.GetAllTransactions()));
            _mockProductTransactionService.Setup(m => m.ConvertTransaction(It.IsAny<string>()))
                                            .Returns(Task.FromResult(ProductTestValue.GetProductTransactionsDto("T2006")));

            InternationalBusinessController controller = new InternationalBusinessController(_mockRateService.Object, _mockTransactionService.Object, _mockProductTransactionService.Object, _mockMapper, _mockLogger.Object);
            //Act
            IActionResult result = await controller.GetProductAllSku("T2006");
            ObjectResult dtoAsObjectResult = result as ObjectResult;
            var dtoResult = dtoAsObjectResult.Value as ProductModel;

            // Assert
            Assert.NotNull(dtoAsObjectResult);
            Assert.Equal(StatusCodes.Status200OK, dtoAsObjectResult.StatusCode);
            Assert.Equal(2, dtoResult.transaction.Count());
        }

        [Fact]
        public async Task GetProductTransactionsAsync_ReturnErrorIActionResult()
        {
            //Arrange
            _mockRateService.Setup(m => m.GetAll())
                                         .Returns(Task.FromResult(RatesTestValue.GetAllRates()));
            _mockTransactionService.Setup(m => m.GetAll())
                                        .Returns(Task.FromResult(TransactionTestValue.GetAllTransactions()));
            _mockProductTransactionService.Setup(m => m.ConvertTransaction(It.IsAny<string>()))
                                       .Throws(new Exception());

            InternationalBusinessController controller = new InternationalBusinessController(_mockRateService.Object, _mockTransactionService.Object, _mockProductTransactionService.Object, _mockMapper, _mockLogger.Object);
            //Act
            var result = await controller.GetProductAllSku("T2006");
            var internalServerErrorCode = result as StatusCodeResult;
            //Assert
            Assert.NotNull(result);
            Assert.Equal(result.GetType(), typeof(StatusCodeResult));
            Assert.Equal(StatusCodes.Status500InternalServerError, internalServerErrorCode.StatusCode);
        }
    }
}