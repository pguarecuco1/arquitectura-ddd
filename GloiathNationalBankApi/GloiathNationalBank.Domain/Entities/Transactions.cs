﻿using GloiathNationalBank.Domain.Common;


namespace GloiathNationalBank.Domain.Entities
{
    public class Transactions : BaseDomainModel
    {
        public string Sku { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public Transactions()
        { }
        public Transactions(string sku, decimal amount, string currency)
        {
            this.Sku = sku;
            this.Amount = amount;
            this.Currency = currency;
        }
    }
}
