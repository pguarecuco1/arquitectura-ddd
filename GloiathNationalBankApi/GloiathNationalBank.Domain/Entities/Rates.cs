﻿using GloiathNationalBank.Domain.Common;

namespace GloiathNationalBank.Domain.Entities
{
    public class Rates: BaseDomainModel
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal Rate { get; set; }

        public Rates()
        { }
        public Rates(string from, string to, decimal rateValue)
        {
            this.From = from;
            this.To = to;
            this.Rate = rateValue;
        }

    }
}
