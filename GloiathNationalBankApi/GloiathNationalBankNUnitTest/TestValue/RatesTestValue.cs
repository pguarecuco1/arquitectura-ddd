﻿using GloiathNationalBank.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBankNUnitTest.TestValue
{
    public class RatesTestValue
    {
        public IEnumerable<RateModel> GetTestRates()
        {
            var rates = new List<RateModel>()
            {
                new RateModel("EUR","USD", 1.359m),
                new RateModel("CAD","EUR", 0.732m),
                new RateModel("USD","EUR", 0.736m),
                new RateModel("EUR","CAD", 0.732m),
                new RateModel("CAD","EUR", 1.366m)
            };

            return rates;
        }
    }
}
