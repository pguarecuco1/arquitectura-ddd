using AutoMapper;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Controllers;
using GloiathNationalBank.Models;
using GloiathNationalBankNUnitTest.TestValue;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GloiathNationalBankNUnitTest
{
    [TestFixture]
    public class Tests
    {
        public IRateService _rateService;
      

        [SetUp]
        public void Setup()
        {
            
        }
      
        [Fact]
        public async Task Test1Async()
        {
            //Arrange
            var prueba= _rateService.GetAll();

            //Act
            var result = _rateService.GetAll();
             var dtoResult = result as IEnumerable<RateModel>;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(5, dtoResult.Count());
        }
    }
}
