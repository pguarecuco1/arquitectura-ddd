﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Contracts.IServices
{
    public interface IRateService
    {
        Task<IEnumerable<Rates>> GetAll();
        Task<IEnumerable<Rates>> GetAllDb();
    }
}
