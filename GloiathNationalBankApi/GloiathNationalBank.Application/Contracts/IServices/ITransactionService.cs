﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Contracts.IServices
{
    public interface ITransactionService
    {
        Task<IEnumerable<Transactions>> GetAll();
        Task<IEnumerable<Transactions>> GetTransactionsByProduct(string productId);
        Task<IEnumerable<Transactions>> GetId(string sku);
    }
}
