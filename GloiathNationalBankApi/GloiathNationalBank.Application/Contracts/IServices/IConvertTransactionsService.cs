﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Contracts.IServices
{
    public interface IConvertTransactionsService
    {
        List<Transactions> ConvertProductTransaction(IEnumerable<Rates> rates, IEnumerable<Transactions> transactions, string currency);
    }
}
