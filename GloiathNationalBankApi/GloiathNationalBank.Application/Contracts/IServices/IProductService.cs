﻿using GloiathNationalBank.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Contracts.IServices
{
    public interface IProductService
    {
        Task<Product> ConvertTransaction(string productId);
    }
}
