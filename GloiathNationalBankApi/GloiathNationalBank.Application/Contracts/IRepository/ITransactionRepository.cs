﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Contracts.IRepository
{
    public interface ITransactionRepository 
    {
        Task<IEnumerable<Transactions>> GetAll();
        Task<IEnumerable<Transactions>> GetId(string sku);
    }
}
