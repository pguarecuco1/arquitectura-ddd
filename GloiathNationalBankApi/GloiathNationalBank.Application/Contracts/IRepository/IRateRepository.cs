﻿using GloiathNationalBank.Domain.Entities;


namespace GloiathNationalBank.Application.Contracts.IRepository
{
    public interface IRateRepository
    {
        Task<IEnumerable<Rates>> GetAll();
        Task<IEnumerable<Rates>> GetAllDb();
    }
}
