﻿using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Models
{
    public class Product
    {
        public List<Transactions> transaction { get; set; }
        public decimal total { get; set; }
    }
}
