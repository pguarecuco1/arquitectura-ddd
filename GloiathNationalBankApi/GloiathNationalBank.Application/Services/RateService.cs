﻿using GloiathNationalBank.Application.Contracts.IRepository;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Services
{
    public class RateService : IRateService
    {
        private readonly IRateRepository _rateRepository;

        public RateService(IRateRepository rateRepository)
        {
            _rateRepository = rateRepository;
        }

        public async Task<IEnumerable<Rates>> GetAll()
        {
            return await _rateRepository.GetAll();
        }

        public async Task<IEnumerable<Rates>> GetAllDb()
        {
            return await _rateRepository.GetAllDb();
        }
    }
}
