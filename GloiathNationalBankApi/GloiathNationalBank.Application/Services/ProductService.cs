﻿using GloiathNationalBank.Application.Constants;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Application.Models;
using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IRateService _rateService;
        private readonly ITransactionService _transactionService;
        private readonly IConvertTransactionsService _convertService;


        public ProductService(ITransactionService transactionService,
            IRateService rateService,
            IConvertTransactionsService exchangeService)
        {
            _rateService = rateService;
            _transactionService = transactionService;
            _convertService = exchangeService;
        }

        public async Task<Product> ConvertTransaction(string sku)
        {
            Product product = new Product();

            var rates = await _rateService.GetAllDb();
            var transactions = await _transactionService.GetId(sku);

            var listProduct= _convertService.ConvertProductTransaction(rates, transactions,CurrencyType.EUR);           

            product.transaction = listProduct;
            product.total = product.transaction.Select(t => t.Amount).Sum();
            return product;
        }
    }
}
