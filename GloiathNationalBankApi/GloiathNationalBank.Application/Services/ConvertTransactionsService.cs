﻿using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Services
{
    public class ConvertTransactionsService : IConvertTransactionsService
    {
        public List<Transactions> ConvertProductTransaction(IEnumerable<Rates> rates, IEnumerable<Transactions> transactions, string currency)
        {
            List<Transactions> listTransactions = new List<Transactions>();
            Transactions infoTransactions = new Transactions();
            foreach (var transaction in transactions)
            {
                infoTransactions = GetProduct(rates, transaction, currency,transaction.Currency);
                listTransactions.Add(infoTransactions);
            }

            return listTransactions.ToList();
        }
        public Transactions GetProduct(IEnumerable<Rates> rates, Transactions transaction, string currency, string currencyAuxi)
        {
            string currencyAux = currencyAuxi;
            var ratesAuxi = rates.SingleOrDefault(x => x.From == currencyAux && x.To==currency);
            if (ratesAuxi != null)
            {
                transaction.Amount *= ratesAuxi.Rate;
                transaction.Amount = Math.Round(transaction.Amount, 2, MidpointRounding.ToEven);

                return transaction;
            }

            var ratesAux = rates.Where(x => x.From == currencyAux);
            if (transaction.Currency == currency)
            {
                transaction.Amount = Math.Round(transaction.Amount, 2, MidpointRounding.ToEven);

                return transaction;
            }

            foreach (var itemRate in ratesAux)
            {
                if (itemRate.To == currency)
                {
                    transaction.Amount *= itemRate.Rate;
                    transaction.Amount = Math.Round(transaction.Amount, 2, MidpointRounding.ToEven);

                    return transaction;
                }
                else
                {
                    transaction.Amount *= itemRate.Rate;
                    var ratesAuxData = rates.SingleOrDefault(x => x.From == itemRate.To && x.To == currency);
                    if (ratesAuxData != null)
                    {
                        transaction.Amount *= ratesAuxData.Rate;
                        transaction.Amount = Math.Round(transaction.Amount, 2, MidpointRounding.ToEven);
                        return transaction;
                    }
                    else
                    {
                       var transacAux= GetProduct(rates, transaction, currency, itemRate.To);
                    }
                }
            }


            return transaction;
        }
    }
}
