﻿using GloiathNationalBank.Application.Contracts.IRepository;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;

        public TransactionService(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public async Task<IEnumerable<Transactions>> GetAll()
        {
            return await _transactionRepository.GetAll();
        }

        public async Task<IEnumerable<Transactions>> GetId(string sku)
        {
            return await _transactionRepository.GetId(sku);
        }

        public async Task<IEnumerable<Transactions>> GetTransactionsByProduct(string productId)
        {
            var transactions = await GetId(productId);
            return transactions.Where(t => t.Sku.ToLower() == productId.ToLower());
        }
    }
}
