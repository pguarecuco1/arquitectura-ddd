﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GloiathNationalBank.Application.Constants
{
    public class CurrencyType
    {
        public const string USD = "USD";
        public const string EUR = "EUR";
        public const string CAD = "CAD";
        public const string AUD = "AUD";
    }
}
