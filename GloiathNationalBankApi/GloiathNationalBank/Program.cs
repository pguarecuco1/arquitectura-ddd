using GloiathNationalBank.Application.Contracts.IRepository;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Application.Services;
using GloiathNationalBank.Infrastructure;
using GloiathNationalBank.Infrastructure.Repository.Providers;
using GloiathNationalBank.Infrastructure.Repository.RatesRepository;
using GloiathNationalBank.Infrastructure.Repository.Transactions;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
var Configuration = builder.Configuration;
// Add services to the container.
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
var connectionString = builder.Configuration.GetConnectionString("connectionString");
builder.Services.AddDbContext<ApplicationDbContext>(x => x.UseSqlServer(connectionString));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IRateService, RateService>();
builder.Services.AddTransient<ITransactionService, TransactionService>();
builder.Services.AddTransient<IProductService, ProductService>();
builder.Services.AddTransient<IConvertTransactionsService, ConvertTransactionsService>();

builder.Services.AddScoped<IRateRepository, RateRepository>();
builder.Services.AddScoped<ITransactionRepository, TransactionRepository>();

builder.Services.AddHttpClient<IRateProviderRepository, RateProviderRepository>(c =>
{
    c.BaseAddress = new Uri(Configuration["Endpoints:Rates"]);
    c.Timeout = new TimeSpan(0, 0, 45);
    c.DefaultRequestHeaders.Clear();
});

builder.Services.AddHttpClient<ITransactionProviderRepository, TransactionProviderRepository>(c =>
{
    c.BaseAddress = new Uri(Configuration["Endpoints:Transactions"]);
    c.Timeout = new TimeSpan(0, 0, 45);
    c.DefaultRequestHeaders.Clear();


});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
