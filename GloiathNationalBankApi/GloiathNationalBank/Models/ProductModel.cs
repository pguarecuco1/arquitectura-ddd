﻿using GloiathNationalBank.Domain.Entities;

namespace GloiathNationalBank.Models
{
    public class ProductModel
    {
        public List<Transactions> transaction { get; set; }
        public decimal total { get; set; }
    }
}
