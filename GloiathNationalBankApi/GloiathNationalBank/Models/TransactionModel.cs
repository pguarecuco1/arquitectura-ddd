﻿namespace GloiathNationalBank.Models
{
    public class TransactionModel
    {
        public string sku { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }

        public TransactionModel()
        { }
        public TransactionModel(string Sku, decimal Amount, string Currency)
        {
            this.sku = Sku;
            this.amount = Amount;
            this.currency = Currency;
        }
    }
}
