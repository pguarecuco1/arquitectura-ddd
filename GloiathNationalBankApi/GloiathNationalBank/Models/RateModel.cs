﻿namespace GloiathNationalBank.Models
{
    public class RateModel
    {
        public string from { get; set; }
        public string to { get; set; }
        public decimal rateValue { get; set; }
        public RateModel()
        { }
        public RateModel(string From, string To, decimal RateValue)
        {
            this.from = From;
            this.to = To;
            this.rateValue = RateValue;
        }

       

    }
}
