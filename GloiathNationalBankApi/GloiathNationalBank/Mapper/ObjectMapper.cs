﻿using AutoMapper;
using GloiathNationalBank.Application.Models;
using GloiathNationalBank.Domain.Entities;
using GloiathNationalBank.Models;

namespace GloiathNationalBank.Mapper
{
    public class ObjectMapper
    {
        /// <summary>
        ///  
        /// </summary>
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => !(!p.GetMethod.IsPublic && !p.GetMethod.IsAssembly);
                cfg.AddProfile<DtoMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        /// <summary>
        ///  
        /// </summary>
        public static IMapper Mapper => Lazy.Value;
    }

    /// <summary>
    ///  
    /// </summary>
    public class DtoMapper : Profile
    {
        /// <summary>
        ///  
        /// </summary>
        public DtoMapper()
        {
            CreateMap<Rates, RateModel>()
 .ForMember(dest => dest.rateValue, opt => opt.MapFrom(src => src.Rate))
 .ForMember(dest => dest.from, opt => opt.MapFrom(src => src.From))
 .ForMember(dest => dest.to, opt => opt.MapFrom(src => src.To))
 .ReverseMap();

            CreateMap<Transactions, TransactionModel>()
.ForMember(dest => dest.sku, opt => opt.MapFrom(src => src.Sku))
.ForMember(dest => dest.amount, opt => opt.MapFrom(src => src.Amount))
.ForMember(dest => dest.currency, opt => opt.MapFrom(src => src.Currency))
.ReverseMap();

            CreateMap<Product, ProductModel>()
.ForMember(dest => dest.total, opt => opt.MapFrom(src => src.total))
.ReverseMap();
        }
    }
}
