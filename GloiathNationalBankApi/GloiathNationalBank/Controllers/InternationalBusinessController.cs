﻿using AutoMapper;
using GloiathNationalBank.Application.Contracts.IServices;
using GloiathNationalBank.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GloiathNationalBank.Controllers
{
    public class InternationalBusinessController : Controller
    {
        private readonly IRateService _rateService;
        private readonly ITransactionService _transactionService;
        private readonly IProductService _productService;
        private readonly IMapper _mapper;
        readonly ILogger<InternationalBusinessController> _logger;

        public InternationalBusinessController(IRateService rateService, ITransactionService transactionService, IProductService productService,
            IMapper mapper,
            ILogger<InternationalBusinessController> logger)
        {
            _rateService = rateService;
            _transactionService=transactionService;
            _productService = productService;
            _mapper = mapper;
            _logger = logger;

        }

        [HttpGet("GetRatesAll")]
        public async Task<IActionResult> GetRatesAll()
        {
            try
            {
                var ratesEntities = await _rateService.GetAll();
                return Ok(_mapper.Map<IEnumerable<RateModel>>(ratesEntities));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed - retrieve rates", ex);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("GetTransactionsAll")]
        public async Task<IActionResult> GetTransactionsAll()
        {
            try
            {
                var transactionEntities = await _transactionService.GetAll();
                return Ok(_mapper.Map<IEnumerable<TransactionModel>>(transactionEntities));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed - retrieve transactions", ex);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpGet("GetProductAllSku")]
        public async Task<IActionResult> GetProductAllSku(string sku)
        {
            if (string.IsNullOrEmpty(sku))
            {
                return BadRequest();
            }

            try
            {
                var transactionEntities= await _productService.ConvertTransaction(sku);
                return Ok(_mapper.Map<ProductModel>(transactionEntities));
            }
            catch (Exception ex)
            {
                _logger.LogError("Failed - retrieve products", ex);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        
    }
}
